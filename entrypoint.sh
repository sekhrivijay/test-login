#!/bin/bash
#
# docker-entrypoint for service

set -e

echo "Executing java ${JAVA_ARGS} "$@""
java ${JAVA_ARGS} -Xms512m -Xmx2096m -jar demo-0.0.1-SNAPSHOT.jar

